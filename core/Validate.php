<?php
	namespace core;
	class Validate
	{
		private $checkError = false;
		private $errors = [];
		const INVALID_EMAIL 	= 'Not mail';
		const INVALID_REQUIRED	= 'Not Required';
		const INVALID_INT 		= 'Invaid int';
		const INVALID_MIN 		= 'Not greater';
		const INVALID_MAX		= 'Not greater than';

		public function make($rules=array(),$messages=array(),$input= array())
		{
			foreach ($input as $key => $value) 
			{
				foreach ($rules as $field => $rule) 
				{
					$arrRules=explode('|', $rule);

					foreach ($arrRules as $type) 
					{
						$arrSubRules 	= explode(':', $type);
						$type 			= $arrSubRules[0];
						$inputField 	= $input[$field];
						$lengthInput 	= strlen($inputField);
						switch ($type) 
						{
							case 'required':
							
								if($inputField == '' || empty($inputField))
								{
									$this->checkError = true;
									$message = isset($messages[$field.'.'.$type]) ? $messages[$field.'.'.$type] : self::INVALID_REQUIRED;
									$this->setError($field, $message);
								}
								break;
							case 'email':
								if(!filter_var($inputField,FILTER_VALIDATE_EMAIL))
								{
									$this->checkError = true;
									$message = isset($messages[$field.'.'.$type]) ? $messages[$field.'.'.$type] : self::INVALID_EMAIL;
									$this->setError($field, $message);
								}
								break;
							case 'min':
								$minDefault = $arrSubRules[1];
								if($lengthInput < $minDefault)
								{
									$this->checkError = true;
									$message = isset($messages[$field.'.'.$type]) ? $messages[$field.'.'.$type] : self::INVALID_MIN;
									$this->setError($field, $message);
								}
								break;
							case 'max':
								$maxDefault = $arrSubRules[1];
								if($lengthInput > $maxDefault)
								{
									$this->checkError = true;
									$message = isset($messages[$field.'.'.$type]) ? $messages[$field.'.'.$type] : self::INVALID_MAX;
									$this->setError($field, $message);
								}
								break;

							case 'int':
								$value = $input[$field];
								if(!is_numeric($value))
								{
									$this->checkError = true;
									$message = isset($messages[$field.'.'.$type]) ? $messages[$field.'.'.$type] : self::INVALID_INT;
									$this->setError($field, $message);
								}
								break;
						}
					}
				}
			}
		}
		public function fails()
		{
			return $this->checkError;
		}

		public function setError($field,$message)
		{
			$this->errors[$field]=$message;
		}

		public function getMessageError()
		{
			return $this->errors;
		}
	}
	
	class Input
	{
		public static function get($name, $type ="str", $default = "")
		{
			$return='';
			if(isset($_POST) && $_POST){
				$return = $_POST[$name];
			}
			else
			{
				$return = $_GET[$name];
			}

			switch ($type) 
			{
				case 'int':
					# code...
				$return = intval($return);
					break;
				case 'str':
					# code...
				$return = strval($return);
					break;
				case 'arr':
					# code...
				$return = array($return);
					break;
				case 'dbl':
					# code...
				$return = boolval($return);
					break;
			}

			$return = $return ? $return : $default;
			return trim($return);
		}

		public static function all()
		{
			return isset($_POST) && $_POST ?  $_POST : $_GET;
		}
	}
?>