<?php
	namespace core\database;
	class DB
	{
		private $host 		= 'localhost';
		private $username 	= 'root';
		private $password	= '';
		private $databaseName ='demo';
		private $charset	= 'utf8';

		private $conn;
		private $result;
		private $table;
		private $select;
		private $where;

		private $statements = array();
		private $pdo;
		private $pdoStatement = null;
		private $tablePrefix = null;

		public function __construct()
		{
			$conn = mysqli_connect($this->host,$this->username,$this->password,$this->databaseName);
			if (!$conn) 
			{	
				throw new Exception("Connection failed: " . mysqli_connect_error());
			}
			$this->conn = $conn;
			mysqli_set_charset($conn,$this->charset);
		}

		public function query($sql,$bindings = array())
		{
			list($this->pdoStatement) = $this->statement($sql,$bindings);
		}

		public function statement($sql,$bindings = array())
		{
			$start = microtime(true);
			$pdoStatement = $this->pdo->prepare($sql);
			foreach ($bindings as $key => $value) {
				$pdoStatement->bindValue(
					is_int($key) ? $key + 1 : $key,
					$value,
					is_int($value) || is_bool($value) ? PDO::PARAM_INT : PDO::PARAM_STR
				); 
			}
			$pdoStatement->execute();
			return array($pdoStatement, microtime(true) - $start);
		}


		public static function table($tablename)
		{
			$that = new self();
			$that->table = $tablename;
			return $that;
		}
		
		public function select()
		{
			$arg_list = func_get_args();
			$this->select[] = $arg_list;
			return $this;
		}

		public function addTablePrefix($values,$tableFieldMix = true)
		{
			if(is_null($this->tablePrefix))
			{
				return $values;
			}
			$single = false;
			if(!is_array($values))
			{
				$values = array($values);
				$single = true;
			}

			$return = array();

			foreach ($values as $key => $value) {
				if($value instanceof Raw || $value instanceof \Closure) //instanceof kiểm tra đối tượng (tương đương is_a())
				{
					$return[$key] = $value;
					continue;
				}
				$target = &$value;
				if(!is_int($key))
				{
					$target = &$key;
				}

				if(!$tableFieldMix || ($tableFieldMix && strpos($target,'.')!==false))
				{
					$target = $this->tablePrefix . $target;
				}
				$return[$key] = $value;
			}
			return $single ? end($return) : $return;
		}

		public function whereHandler($key, $operator = null, $value = null, $joiner = 'AND')
		{
			$key = $this->addTablePrefix($key);
			$this->statements['wheres'][] = compact('key','operator','value','joiner');//compact tạo ra 1 mảng
			return $this;
		}

		public function where($colum='',$operator='',$value='')
		{
			$this->where[] = $colum .$operator. "'".$value."'";
			return $this;
		}

		public function whereIn($key, $values)
		{
			return $this->whereHandler($key, 'IN', $values, 'AND');
		}

		public function whereNotIn($key, $values)
		{
			return $this->whereHandler($key, 'NOT IN', $values, 'AND');
		}
		

		public function whereBetween($key, $valueFrom, $valueTo)
		{
			return $this->whereHandler($key,'BETWEEN', array($valueFrom, $valueTo), 'AND');
		}

		public function orWhereBetween($key, $valueFrom, $valueTo)
		{
			return $this->whereHandler($key,'BETWEEN', array($valueFrom,$valueTo), 'OR');
		}

		public function get() 
		{
			$where = implode(" AND ",$this->where);
			$whereBetweenwh = implode('', $this->whereBetweenwh);
			$whereBetween = implode(" AND ",$this->whereBetween);
			$colum =[];
			$select = $this->select;
			if( is_array($select))
			{
				foreach($select as $val)
				{
					foreach ($val as $field) {
						$colum[] = $field;
					}
				}
			}
			$select = count($colum) > 0 ? implode(",",$colum) : '*';
			$sql=" SELECT $select FROM {$this->table}  WHERE  $whereBetweenwh BETWEEN $whereBetween";
			// $sql=" SELECT $select FROM {$this->table}";
			// $sql=" SELECT $select FROM {$this->table} WHERE $where";
			// var_dump($sql);

			$result = mysqli_query($this->conn,$sql);
			$arr = [];
			if(mysqli_num_rows($result)>0)
			{
				while($row = mysqli_fetch_assoc($result))
				{
					$arr[] = $row;
				}
			}
			return $arr;
		}

	}
?>
