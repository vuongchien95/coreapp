<?php
	if(!function_exists('first_error'))
	{
		function first_error($name)
		{
			$messages = core\Session::get('errors');
			$error = '';
			if(count($messages) > 0)
			{
				$error = isset($messages[$name]) 
							? '<span class="text-danger help-block">'.$messages[$name] .'</span>' 
							: '';
			}	
			return $error;
		}
	}

	if(!function_exists('has_validate'))
	{
		function has_validate($name)
		{
			
			$messages = core\Session::get('errors');
			$error = '';
			if(count($messages) > 0)
			{
				$error = isset($messages[$name]) 
							? 'has-error' 
							: '';
			}	
			return $error;
		}
	}
?>