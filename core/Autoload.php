<?php
class Autoload
{
	public function __construct()
	{
		spl_autoload_register(array($this, 'loader'));
		require(ROOT.DS.'core/function/function.php');
	}

	private function loader($className) 
	{
        $pathName = ROOT.DS.$className.'.php';
        if (is_readable($pathName)) 
        {
        	require($pathName);
        }
        else
        {
        	throw new Exception("$pathName not found");
        	
        }
    }
}

?>