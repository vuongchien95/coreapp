<?php
    namespace core;
    class Session {
        public static function exits($key) 
        {
            return (isset($_SESSION[$key])) ? true : false;
        }

        public static function set($key, $val)
        {
            return $_SESSION[$key] = $val;
        }

        public static function get($key)
        {
            return self::exits($key) ? $_SESSION[$key] : null;
        }

        public static function delete($key)
        {
    		return self::exits($key) ? session_unset($_SESSION[$key]) : null;
        }
    }

?>
