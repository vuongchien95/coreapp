<?php
	require_once('config.php');
	new core\Validate();
    // $db = new Database(); 
    // $product = core\database\DB::table('tb_user')
    //                         ->where('id','=','6')
    //                         ->whereBetween('id',10,13);
    //                     // ->get();

    // echo "<pre>";
    // var_dump($product) ;


	if(isset($_POST['action']))
	{
		$username = Input::get('username');
		$password = Input::get('password');
		$email 	  = Input::get('email');
		$fullname = Input::get('fullname');
		$phone	  = Input::get('phone');	
		$message=[
			'username.required' => 'Vui lòng nhập username',
			'username.min' 		=> 'Tối thiểu 6 ký tự',
			'username.max'		=> 'Tối đa 20 ký tự',
			'password.required' => 'Vui lòng nhập password',
			'password.min'		=> 'Password tối thiểu 6 ký tự',
			'email.required'	=> 'Vui lòng nhập email',
			'email.email'		=> 'Email không hợp lệ',
			'fullname.required'	=> 'Vui lòng nhập họ và tên',
			'phone.required'	=> 'Vui lòng nhập số điện thoại',
			'phone.int'			=> 'Vui lòng nhập số',
			'phone.min'			=> 'Số điện thoại không hợp lệ'
		];
		$rules = [
			'username' 	=> 'min:6|max:20|required',
			'email'		=> 'email|required',
			'password' 	=> 'min:6|required',
			'fullname'	=> 'required',
			'phone'		=> 'int|min:11|required'
		];

		$validate->make($rules,$message,Input::all());
		if($validate->fails())
		{
			$messageError = $validate->getMessageError();
			Session::set('errors',$messageError);
		}
        else
		{
			$data = [
                'username'  => $username,
                'password'  => md5($password),
                'email'     => $email,
                'fullname'  => $fullname,
                'phone'     => $phone
            ];
			$db = new Database();
            $db->insert('tb_user',$data);
			echo 'Bạn đã đăng ký thành công. ID của bạn là: '.$db->getLastId();
			Session::delete('errors');
		}
	}
?>
<html>
<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <title>Form đăng ký</title>
     <link rel="stylesheet" href="">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
     <script src="//code.jquery.com/jquery.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
 </head>
 
 <body>
     <div class="container">
         <h2 class="text-center">ĐĂNG KÝ</h2>

         <form method="POST" accept-charset="utf-8" class="form-horizontal">
             <div class="form-group <?php echo has_validate('username')?>">
                 <label class="control-label">Username:</label>
                 <input type="text" name="username" class="form-control" value="<?php if (isset($username)) {echo $username;} ?>">
                 <?php echo first_error('username') ?>
             </div>
             <div class="form-group <?php echo has_validate('email')?>">
                 <label class='control-label'>Password:</label>
                 <input type="password" name="password" class="form-control">
                 <?php echo first_error('password') ?>
             </div>
             <div class="form-group <?php echo has_validate('email')?>">
                 <label class='control-label'>Email:</label>
                 <input type="text" name="email" class="form-control">
                 <?php echo first_error('email') ?>
             </div>
             <div class="form-group <?php echo has_validate('fullname')?>">
                 <label class='control-label'>Họ và tên:</label>
                 <input type="text" name="fullname" class="form-control">
                 <?php echo first_error('fullname') ?>
             </div>
             <div class="form-group <?php echo has_validate('phone')?>">
                 <label class='control-label'>Số điện thoại:</label>
                 <input type="text" name="phone" class="form-control">
                 <?php echo first_error('phone') ?>
             </div>
             <button type="submit" class="btn btn-info" style="margin-top: 15px;" name="action">Resgister</button>
         </form>
 </body>
 </html>