<?php
	namespace core\database;
	class Database
	{
		private $host		= 'localhost';
		private $username	= 'root';
		private $password	= '';
		private $databaseName = 'demo';
		private $charset	= 'utf8';

		private $conn;
		private $result;

		public function __construct($config = array())
		{
			if($config)
			{
				$this->host 		= $config['host'];
				$this->username 	= $config['username'];
				$this->password 	= $config['password'];
				$this->databaseName = $config['databaseName'];
			}
			$this->connect();
		}

		public function connect()
		{
			$conn = mysqli_connect($this->host,$this->username,$this->password,$this->databaseName);
			if (!$conn) 
			{	
				throw new NotConnectDatabaseException("Connection failed: " . mysqli_connect_error());
			}
			$this->conn = $conn;
			mysqli_set_charset($conn,$this->charset);
		}

		public function db_query($sql)
		{
			$this->result = mysqli_query($this->conn,$sql);
		}
		/**
	     * insert thêm dữ liẹu
	     * @param string $table tên bảng muốn thêm, array $data dữ liệu cần thêm
	     * @return boolean
	    */
	    public function insert($table = '', $data = [])
	    {
	        $keys = '';
	        $values= '';
	        foreach ($data as $key => $value) {
	            $keys .= ',' . $key;
	            $values .= ',"' . mysqli_real_escape_string($this->conn,$value).'"';
	        }
	        $sql = 'INSERT INTO ' .$table . '(' . trim($keys,',') . ') VALUES (' . trim($values,',') . ')';
	        return mysqli_query($this->conn,$sql);
	    }
	    /**
	     * update sửa dữ liệu
	     * @param string $table tên bảng muốn sửa, array $data dữ liệu cần sửa, array|int $id điều kiện
	     * @return boolean
	     */
	    public function update($table = '',$data = [], $id =[])
	    {
	        $content = '';
	        if(is_integer($id))
	            $where = 'id =' .$id;
	        else if(is_array($id) && count($id)==1){
	            $listKey = array_keys($id);
	            $where = $listKey[0].'='.$id[$listKey[0]];
	        }
	        else
	            die('Không thể có nhiều hơn 1 khóa chính và id truyền vào phải là số');
	        foreach ($data as $key => $value) {
	            $content .= ','. $key . '="' . mysqli_real_escape_string($this->conn,$value).'"';
	        }
	        $sql = 'UPDATE ' .$table .' SET '.trim($content,',') . ' WHERE ' . $where ;
	        return mysqli_query($this->conn,$sql);
	    }
	    /**
	     * delete xóa dữ liệu
	     * @param string $table tên bảng muốn xóa, array|int điều kiện
	     * @return boolean
	     */
	    public function delete($table= '', $id = [])
	    {
	        $content = '';
	        if(is_integer($id))
	            $where = 'id = '.$id;
	        else if(is_array($id) && count($id)==1){
	            $listKey = array_keys($id);
	            $where = $listKey[0].'='.$id[$listKey[0]];
	        }
	        else
	            die('Không thể có nhiều hơn 1 khóa chính và id truyền vào phải là số');
	        $sql = 'DELETE FROM ' . $table . ' WHERE '. $where;
	        return mysqli_query($this->conn,$sql);
	    }
	    /**
	     * getObject lấy hết dữ liệu trong bảng trả về mảng đối tượng
	     * @param string $table tên bảng muốn lấy ra dữ liệu
	     * @return array objetc
	     */
	    public function query($sql ='', $return = true)
	    {
	        if($result = mysqli_query($this->conn,$sql))
	        {
	            if($return === true){
	                while ($row = mysqli_fetch_assoc($result)) {
	                    $data[] = $row;
	                }
	                mysqli_free_result($result);
	                return $data;
	            }
	            else
	                return true;
	        }
	        else
	            return false;
	    }

		public function getLastId()
		{
			return mysqli_insert_id($this->conn);
		}

		public function fetchAll()
		{
			$arr = [];
			if(mysqli_num_rows($this->result)>0)
			{
				while($row = mysqli_fetch_assoc($this->result))
				{
					$arr[] = $row;
					// echo "id: " .$row["id"] . " - Ho ten: " .$row["hoten"] . "<br>";
				}
			}
			return $arr;
		}
		// mysql_fetch_row () - Nhận một dòng kết quả như là một mảng liệt kê
		// mysql_fetch_assoc () - Lấy ra một dòng kết quả như một mảng kết hợp
		public function count()
		{
			return mysqli_fetch_assoc($this->result);
		}
		
	}
?>
<?php
	// $sql = "DELETE FROM `tb_hocsinh` WHERE `id`=3";
	// $sql = "INSERT INTO `tb_hocsinh`(`hoten`, `lop`, `email`, `phone`, `giotinh`) VALUES ('Nguyễn Văn B','Tk11.2','nguyenb@gmail.com','0987654321','nam')";
	// $sql = "UPDATE `tb_hocsinh` 
	// 		SET `hoten`='Vương Đình Chiến',`lop`='Tk11.1',`email`='nhocvip8xxx@gmail.com',`phone`='01648485882' WHERE `id`=2";
	// $sql = "SELECT COUNT(*) AS 'Bản Ghi' FROM tb_hocsinh";
	// $db = new Database($sql);
	// var_dump(Input::get('name','int', '9'));
?>